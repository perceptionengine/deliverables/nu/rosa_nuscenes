# Rosa Nuscenes Datasets

## Setup

`pip install nuscenes-devkit`

## Load

```
$ python
> from nuscenes.nuscenes import NuScenes
> DEEPEN_NUSCENES_ROOT = '/PATH/TO/ROS_NUSCENES/'
> nusc = NuScenes(version='v1.0-deepen', dataroot=DEEPEN_NUSCENES_ROOT, verbose=True)
> print(nusc.list_scenes())
> print(nusc.list_categories())
```

## Notebook Example

[Rosa](./RosaNuScenesExample.ipynb)

**The name of the scene matches the original Rosbag name**